import 'package:mobile_midterm/mobile_midterm.dart' as mobile_midterm;
import 'dart:io';
void main(List<String> arguments) {
  print('Please input mathematical expression');
  var string = stdin.readLineSync()!;
  var infix = tokenizing(string);
  print('Infix: $infix');
  var postfix = checkPostfix(infix);
  print('Postfix: $postfix');
  var result = evaluatePostfix(postfix);
  print('result after evaluate postfix: $result');
}

List tokenizing(String string) {
  List result = [];
  for (var i = 0; i < string.length; i++) {
    if (string[i] == ' ') {
      result.add(string[i]);
      result.remove(string[i]);
    } else {
      result.add(string[i]);
    }
  }
  result = tokenizingNegative(result);
  return result;
}

List tokenizingNegative(List lst) {
  for (var i = 0; i < lst.length - 1; i++) {
    for (var j = i + 1; j < lst.length; j++) {
      if (lst[i] == '-' && lst[j] == '(') {
        break;
      } else if (lst[i] == '-') {
        var temp = lst[j];
        lst[i] = '-$temp';
        lst.remove(lst[j]);
      }
    }
  }
  return lst;
}

bool isInteger(String str) {
  if (str == null) {
    return false;
  }
  return double.tryParse(str) != null;
}

int precedence(String operator) {
  if (operator == "(" || operator == ")") {
    return 4;
  }
  if (operator == "^") {
    return 3;
  }
  if (operator == "*" || operator == "/") {
    return 2;
  }
  if (operator == "%") {
    return 3;
  }
  if (operator == "+" || operator == "-") {
    return 1;
  }
  return 0;
}

List checkPostfix(List infix) {
  List operators = [];
  List postfix = [];
  infix.forEach((i) {
    if (isInteger(i)) {
      postfix.add(i);
    } else if (!isInteger(i) && i != '(' && i != ')') {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          precedence(i) < precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(i);
    } else if (i == '(') {
      operators.add(i);
    } else if (i == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.remove('(');
    }
  });
  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

double evaluatePostfix(List postfix) {
  List values = [];
  double left, right;
  postfix.forEach((i) {
    if (isInteger(i)) {
      double temp = double.parse(i);
      values.add(temp);
    } else {
      right = values.removeLast();
      left = values.removeLast();
      var result = calculator(left, right, i);
      values.add(result);
    }
  });
  return values.first;
}

double calculator(double left, double right, String operator) {
  double result = 0;
  switch (operator) {
    case "+":
      {
        result = left + right;
      }
      break;
    case "-":
      {
        result = left - right;
      }
      break;
    case "*":
      {
        result = left * right;
      }
      break;
    case "/":
      {
        result = left / right;
      }
      break;
    case "%":
      {
        result = left % right;
      }
      break;
    case "^":
      {
        result = exponent(left, right);
      }
      break;
    default:
      {}
      break;
  }

  return result;
}

double exponent(double left, double right) {
  double expo = left;
  double result = 1;
  for (int i = 0; i < right; i++) {
    result = result * expo;
  }

  return result;
}

